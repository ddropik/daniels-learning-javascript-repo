/*
This is file is used to demonstrate objects.

 An object is collection of related data(variables a.k.a. "properties") & behavior (functions)
*/


var player1 = new Object(); //creates object container called "player1", using long-hand.
player1.name="fred"; //using "dot-syntax" to create a property of the player1 object, and assign it a value.
player1.score=1000;
player1.rank=1; 

var player2 = {name:"josh", score:20, rank:4}; //object creation in shorthand



function playerDetails() { //since this function is associated with different ojbects use "this"
    console.log(" the name of the object is  : "+this.name )
}

player1.logDetails = playerDetails; //associating the function with object
player2.logDetails = playerDetails; //associating the function with a different object

player1.logDetails(); //calling the function so it executes
