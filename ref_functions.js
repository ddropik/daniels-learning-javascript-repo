/*
This is about functions. Functions group together statements.

Best practice is to declare functions before calling them, top of javascript file.
*/




/******************* Function without Parameters  *************************************************************
************************************************************************************************

function myFunction () {       //Declaring function
   
    console.log("hello");
    //statement code
    //statement code
}

myFunction ();    //calls the function named myFunction 
myFunction ();



***********NOTE: 
**********************************************************************************************/






/******************* Function with Parameters  *************************************************************
************************************************************************************************

function myFunction (x,y) {       //Declaring function
    var myVar = x+y;
    console.log(myVar);
}

myFunction (6,6);    //calls the function named myFunction, passes in parameters 6 and 6 
myFunction (7,3);



***********NOTE: 
**********************************************************************************************/


/****************** Function with Parameters & Return value  ************************************************
************************************************************************************************

function myFunction (x,y) {       //Declaring function
    var myVar = x+y;
    console.log(myVar);

    return myVar;   //returns a variable, or number, string literal 
}

myFunction (6,6);    //calls the function named myFunction, passes in parameters 6 and 6 
myFunction (7,3);
var myresult = myFunction(6,9);  //assign variable to value of function with these given paramaters, this only works b/c you have a return value



***********NOTE: 
**********************************************************************************************/


/****************** variable scope  ************************************************
************************************************************************************************
var bar; // a global variable, is declared outside the function, exists anywhere

function myFunction (x,y) {       
    var foo = 500;  //local variable, only exists inside the function
    console.log(foo);
}

myFunction();
	console.log(foo); // WARNING!!, foo is undefined b/c it is only a local variable

	console.log(bar); // this works fine b/c "bar" is global


***********NOTE: variables in loops, if-statements, etc. are NOT local variables
**********************************************************************************************/