
/****************** array  ************************************************
************************************************************************************************
**********************************************************************************************/

var sampleArray = [];  //creates a variable that is an array. A Long-hand form.

sampleArray[0]=50;          // sets value at index 0 to be the integer 50
sampleArray[1]="string";    // sets value at index 1 to be the string "string"

console.log(sampleArray[0]); // gets value at index 0 and outputs it to the console 



var anotherArray = [50,"string"]; // short-hand. create variable that is an array. set initial values.

var yetAnotherArray = new Array(); // Another Long-hand form. create an array.

var yetAnotherArray2 = new Array(5); // create array with 5 elements

/****************** array properties ************************************************
**********************************************************************************************/

// .length property

	console.log(sampleArray.length);  //tells how many elements are in this array

/****************** array methods ************************************************
**********************************************************************************************/

//.reverse

	var reversedArray = sampleArray.reverse(); //creates a new array that is the reverse of sampleArray, assigns it to variable

//.join
	console.log(reversedArray.join()); //join values in array together and output it as a string

// .sort
